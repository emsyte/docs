# Emsyte Project

## Overview


| Screenshots |  |
| ------ | ------ |
| ![Webinar Analysis Page](analysis_page_cropped.png)       |  ![Webinar Moderator Panel](02_Moderator.png)      |
| ![Webinar Viewer](01_Viewer.png)       |  ![logo](JustWebinar_LogoColor_Lgt.svg)      |



This project is mostly split between two repositories: The backend [emsyte-api](https://gitlab.com/emsyte/emsyte-api) and the frontend [emsyte-client-new](https://gitlab.com/emsyte/emsyte-client-new)

The frontend is written in ReactJS with typescript. The backend is written in typescript and deployed to AWS using the [serverless](https://serverless.com) framework. Check the READMEs of each project for more details.

## Getting started

To get started with the emsyte project you'll need to first clone these git repositories:

* https://gitlab.com/emsyte/emsyte-api - Main backend
* https://gitlab.com/emsyte/emsyte-client-new - ReactJS Frontend
* https://gitlab.com/emsyte/grapesjs-plugin - GrapesJS plugin used for template editor
* https://gitlab.com/emsyte/emsyte-common - Common libraries shared between the front-end and backend


Then, in `emsyte-api` and `emsyte-client-new` you'll need to run `yarn install` to install the dependencies.

### Linking extra dependencies

After cloning down all the repositories and installing the dependencies, you'll need to link the extra dependencies. If you're already able to run `yarn start` in both the frontend and backend repositories you can skip this step.

There are two ways of linking the extra dependencies, using git ssh, or using `yarn link`. If you have ssh [configured](https://docs.gitlab.com/ee/ssh/) to work with gitlab.com then you should be good to go. Otherwise, you'll need to link the dependencies.

To link the extra dependencies, you can run `yarn link` in both the `emsyte-common` repository and the `grapesjs-plugin` repository.


    cd emsyte-common
    yarn link
    cd ../grapesjs-plugin
    yarn link


Then, you need to link those repositories in the frontend and backend as follows:


    # Link dependencies for frontend
    cd emsyte-client-new
    yarn link "@emsyte/common"
    yarn link "@emsyte/grapesjs-plugin"

    # Link dependencies for backend (note, the grapesjs-plugin repo isn't used by the backend)
    cd ../emsyte-api
    yarn link "@emsyte/common"


### Running the project

There are a few prerequisites before you can start the backend project:

* Docker (used to start a postgresql database)
* AWS access keys (when running locally it won't try to communicate with AWS at all, but the serverless framework will crash without *something* defined even when running locally)

To configure the AWS access keys, you can add this to `~/.aws/credentials`:

```
[default]
aws_access_key_id = 
aws_secret_access_key = 
```
(You can also use [environment variables](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html))

After setting up the AWS credentials, you can run `yarn start` to start the backend. It will take awhile the first time because it will create a new database and pre-populate it with some data.


To run the frontend all you need to do is run `yarn start`.

You can then access the project at http://localhost:3000

To login, use these credentials:

    Email: johndoe1@example.com
    Password: access
